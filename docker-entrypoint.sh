#!/bin/sh -e
#
# QUBIK Listener Docker startup script
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

run_listener() {
	exec qubik_listener.py \
	     ${CALLSIGN:+ --callsign "${CALLSIGN}"} \
	     ${LAT:+ --lat "${LAT}"} \
	     ${LON:+ --lon "${LON}"} \
	     ${UDP_IP:+ --udp_ip "${UDP_IP}"} \
	     ${UDP_PORT:+ --udp_port "${UDP_PORT}"} \
	     ${TELEMETRY_URL:+ --telemetry_url "${TELEMETRY_URL}"}
}

run_transceiver() {
	exec qubik_transceiver.py \
	     ${ANTENNA_RX:+ --antenna-rx "${ANTENNA_RX}"} \
	     ${ANTENNA_TX:+ --antenna-tx "${ANTENNA_TX}"} \
	     ${BAUDRATE:+ --baudrate "${BAUDRATE}"} \
	     ${BB_FREQ:+ --bb-freq "${BB_FREQ}"} \
	     ${BW:+ --bw "${BW}"} \
	     ${DC_REMOVAL:+ --dc-removal "${DC_REMOVAL}"} \
	     ${DECODED_DATA_FILE_PATH:+ --decoded-data-file-path "${DECODED_DATA_FILE_PATH}"} \
	     ${DEV_ARGS:+ --dev-args "${DEV_ARGS}"} \
	     ${DOPPLER_CORRECTION_PER_SEC:+ --doppler-correction-per-sec "${DOPPLER_CORRECTION_PER_SEC}"} \
	     ${ENABLE_IQ_DUMP:+ --enable-iq-dump "${ENABLE_IQ_DUMP}"} \
	     ${FILE_PATH:+ --file-path "${FILE_PATH}"} \
	     ${GAIN_MODE:+ --gain-mode "${GAIN_MODE}"} \
	     ${GAIN_RX:+ --gain-rx "${GAIN_RX}"} \
	     ${GAIN_TX:+ --gain-tx "${GAIN_TX}"} \
	     ${INTERVAL_TX:+ --interval-tx "${INTERVAL_TX}"} \
	     ${IQ_FILE_PATH:+ --iq-file-path "${IQ_FILE_PATH}"} \
	     ${LO_OFFSET:+ --lo-offset "${LO_OFFSET}"} \
	     ${OSDLP_IP:+ --osdlp-ip "${OSDLP_IP}"} \
	     ${OTHER_SETTINGS:+ --other-settings "${OTHER_SETTINGS}"} \
	     ${PPM:+ --ppm "${PPM}"} \
	     ${QUBIK_LISTENER_IP:+ --qubik-listener-ip "${QUBIK_LISTENER_IP}"} \
	     ${QUBIK_LISTENER_PORT:+ --qubik-listener-port "${QUBIK_LISTENER_PORT}"} \
	     ${RIGCTL_HOST:+ --rigctl-host "${RIGCTL_HOST}"} \
	     ${RIGCTL_PORT:+ --rigctl-port "${RIGCTL_PORT}"} \
	     ${RX_FREQ:+ --rx-freq "${RX_FREQ}"} \
	     ${SAMP_RATE:+ --samp-rate "${SAMP_RATE}"} \
	     ${SOAPY_RX_DEVICE:+ --soapy-rx-device "${SOAPY_RX_DEVICE}"} \
	     ${STREAM_ARGS:+ --stream-args "${STREAM_ARGS}"} \
	     ${TUNE_ARGS:+ --tune-args "${TUNE_ARGS}"} \
	     ${TX_FREQ:+ --tx-freq "${TX_FREQ}"} \
	     ${WATERFALL_FILE_PATH:+ --waterfall-file-path "${WATERFALL_FILE_PATH}"}
}

run_receiver() {
	exec qubik_receiver.py \
	     ${ANTENNA_RX:+ --antenna-rx "${ANTENNA_RX}"} \
	     ${BAUDRATE:+ --baudrate "${BAUDRATE}"} \
	     ${BB_FREQ:+ --bb-freq "${BB_FREQ}"} \
	     ${BW:+ --bw "${BW}"} \
	     ${DC_REMOVAL:+ --dc-removal "${DC_REMOVAL}"} \
	     ${DECODED_DATA_FILE_PATH:+ --decoded-data-file-path "${DECODED_DATA_FILE_PATH}"} \
	     ${DEV_ARGS:+ --dev-args "${DEV_ARGS}"} \
	     ${DOPPLER_CORRECTION_PER_SEC:+ --doppler-correction-per-sec "${DOPPLER_CORRECTION_PER_SEC}"} \
	     ${ENABLE_IQ_DUMP:+ --enable-iq-dump "${ENABLE_IQ_DUMP}"} \
	     ${FILE_PATH:+ --file-path "${FILE_PATH}"} \
	     ${GAIN_MODE:+ --gain-mode "${GAIN_MODE}"} \
	     ${GAIN_RX:+ --gain-rx "${GAIN_RX}"} \
	     ${IQ_FILE_PATH:+ --iq-file-path "${IQ_FILE_PATH}"} \
	     ${LO_OFFSET:+ --lo-offset "${LO_OFFSET}"} \
	     ${OSDLP_IP:+ --osdlp-ip "${OSDLP_IP}"} \
	     ${OTHER_SETTINGS:+ --other-settings "${OTHER_SETTINGS}"} \
	     ${PPM:+ --ppm "${PPM}"} \
	     ${QUBIK_LISTENER_IP:+ --qubik-listener-ip "${QUBIK_LISTENER_IP}"} \
	     ${QUBIK_LISTENER_PORT:+ --qubik-listener-port "${QUBIK_LISTENER_PORT}"} \
	     ${RIGCTL_HOST:+ --rigctl-host "${RIGCTL_HOST}"} \
	     ${RIGCTL_PORT:+ --rigctl-port "${RIGCTL_PORT}"} \
	     ${RX_FREQ:+ --rx-freq "${RX_FREQ}"} \
	     ${SAMP_RATE:+ --samp-rate "${SAMP_RATE}"} \
	     ${SOAPY_RX_DEVICE:+ --soapy-rx-device "${SOAPY_RX_DEVICE}"} \
	     ${STREAM_ARGS:+ --stream-args "${STREAM_ARGS}"} \
	     ${TUNE_ARGS:+ --tune-args "${TUNE_ARGS}"} \
	     ${WATERFALL_FILE_PATH:+ --waterfall-file-path "${WATERFALL_FILE_PATH}"}
}

. ~/.qubik-listener-virtualenv/bin/activate

case "$1" in
	listener)
		run_listener
		;;
	transceiver)
		if [ "${MODE}" = "RX" ]
		then
			run_receiver
		else
			run_transceiver
		fi
		;;
esac

exec "$@"
