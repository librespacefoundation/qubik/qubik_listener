# QUBIK transceiver Docker image
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG GNURADIO_IMAGE_TAG=3.8.2.0
FROM librespace/gnuradio:${GNURADIO_IMAGE_TAG}

ARG QUBIK_COMMS_SW_VERSION=v2.0.0

# Download flowgraphs
RUN TMP_DIR="$(mktemp -d)" \
    && curl -s -L "https://gitlab.com/librespacefoundation/qubik/qubik-comms-sw/-/archive/${QUBIK_COMMS_SW_VERSION}/qubik-comms-sw-${QUBIK_COMMS_SW_VERSION}.tar.gz" | \
    tar -C "$TMP_DIR" -xz --strip-components=3 "qubik-comms-sw-${QUBIK_COMMS_SW_VERSION}/test/flowgraphs/" \
    && grcc -o /usr/local/bin "$TMP_DIR"/*.grc \
    && rm -r "$TMP_DIR"

# Install virtualenv package
RUN apt-get update \
    && apt-get install -qy virtualenv soapysdr-module-rtlsdr\
    && rm -r /var/lib/apt/lists/*

# Copy source code and scripts
COPY . /usr/local/src/qubik-listener
COPY qubik_listener.py /usr/local/bin/
COPY contrib/start_qubik.sh /usr/local/bin/

# Create virtualenv
RUN virtualenv \
    --system-site-packages \
    ~/.qubik-listener-virtualenv \
    && ~/.qubik-listener-virtualenv/bin/pip install -r /usr/local/src/qubik-listener/requirements.txt

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

# Expose transceiver ports
EXPOSE 16881/udp
EXPOSE 16882/udp

# Run application
CMD ["listener"]
